package com.allstate.dao;

import java.util.List;

import com.allstate.entities.Employee;

public interface db {
    int count();
    Employee  find();
    List<Employee> findAll();
}
