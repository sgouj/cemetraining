/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package com.allstate;

import com.allstate.entities.Person;

public class App {
    public String getGreeting() {
        return "Hello world.";
    }

    public static void main(String[] args) {
        // System.out.println(new App().getGreeting());
        // Person p = new Person();
        // p.setName("Sandeep");
        // p.setId(28);
        // p.setAge(30);
        // p.dispalay();
        // p = null;
        // Person p1 =new Person(1,"sandy",50);
        // p1.dispalay();

        // Person [] p = new Person[2];
        //  p[0] = new Person(1,"Sandeep", 29);
        //  p[1] = new Person(2,"Sandy", 29);
        //   for (Person person : p) {
        //       person.dispalay();
        //   }      
        System.out.println(Person.getAgeLimit());
    }
}
