package com.allstate.labs;

public class CurrentAccount extends Accounts {

    @Override
    public void addInterest() {
        System.out.println("Current Account addInterest");
        setBalance(getBalance() * 1.1);
        System.out.println(getBalance());
    }

    public CurrentAccount(String name, double balance) {
        super(name, balance);
    }
    
}
