package com.allstate.labs;

public class SavingAccount extends Accounts{

    @Override
    public void addInterest() {
        System.out.println("Saving Account addInterest");
        setBalance(getBalance() * 1.4);
        System.out.println(getBalance());
    }

    public SavingAccount(String name, double balance) {
        super(name, balance);
    }
    
}
