package com.allstate.labs;

public class TestInheritance {
public static void main(String[] args) {
    Accounts [] accounts = new Accounts[3];
    accounts[0] = new SavingAccount("Account", 50);
    accounts[1] = new SavingAccount("Saving Account", 100);
    accounts[2] = new CurrentAccount("Current Account", 200);
    for (Accounts accounts2 : accounts) {
        accounts2.addInterest();
    } 
}   
}
