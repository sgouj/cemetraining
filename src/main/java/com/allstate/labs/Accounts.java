package com.allstate.labs;

public abstract class Accounts {
    private double balance;
    private String name;
    private static double interestRate = 7.25;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // public void addInterest() {
    //     System.out.println("Account AddInterest");
    //     System.out.println(this.balance += this.balance * (getInterestRate()/100)); 
    // }

    public abstract void addInterest();

    public Accounts() {
        this.name = "Sandeep";
        this.balance = 50;
    }

    public Accounts (String name, double balance) {
        this.balance = balance;
        this.name = name;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Accounts.interestRate = interestRate;
    }

    public boolean withdraw(double amount) {
      return balance > amount;
    }

    public boolean withdraw() {
          balance -= 20;
          return balance > 0;
    };
}
