package com.allstate.entities;

public class Employee extends Person {
    private String email;
    private double salary;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Employee(String email, double salary) {
        this.email = email;
        this.salary = salary;
    }

    public Employee(int id, String email, double salary) {
        super(id);
        this.email = email;
        this.salary = salary;
    }

    public Employee(int id, String name, int age, String email, double salary) {
        super(id, name, age);
        this.email = email;
        this.salary = salary;
    }

    @Override
    void printf() {

    }

 public void dispaly() {
     System.out.println(this.email+" "+this.salary);
 }
    
}
