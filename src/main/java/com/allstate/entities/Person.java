package com.allstate.entities;

public class Person {
    private int id;
    private  String name;
    private int age;
    //Quiz final is constant and can't sub class it.
    private final static int ageLimit=100;


    public static int getAgeLimit() {
        return ageLimit;
    }

    public Person() {

    }
    public Person(int id) {
        this.id =id;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void dispalay() {
        System.out.printf("value are %s,%d", this.name, this.id);
    }

    protected void print() {

    }

    void printf() {

    }

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }


}
