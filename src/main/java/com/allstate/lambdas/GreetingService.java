package com.allstate.lambdas;

public interface GreetingService {
    public void sayMessage(String message);
}
