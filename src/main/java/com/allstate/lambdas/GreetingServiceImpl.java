package com.allstate.lambdas;

import java.util.function.Function;

@FunctionalInterface
interface numberOperation {
    public int add(int a, int b);
}

public class GreetingServiceImpl {
    public static void main(String[] args) {

    GreetingService greeting  = (message) -> {
        System.out.println(message);
    };

    greeting.sayMessage("sandeep");

    numberOperation op = (a,b) -> {
        return a+b;
    };

    int ans = op.add(5, 10);
    System.out.println(ans);

    Function<Integer, String> myFunc = (a) -> {
        System.out.println(a);
        return "Finish";
    };
    myFunc.apply(10);

}
    
}
