package com.allstate.lambdas;

import java.util.ArrayList;

import com.allstate.entities.Employee;

public class TestCollections {
    public static void main(String[] args) {
    ArrayList<Employee> al= new ArrayList<Employee>();
    Employee e1 = new Employee("sandeep", 1000);
    Employee e2 = new Employee("test", 2000);
    al.add(e1);
    al.add(e2);
    for (Employee employee : al) {
        employee.dispalay();
    }
    
 }
}