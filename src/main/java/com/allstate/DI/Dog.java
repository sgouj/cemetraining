package com.allstate.DI;

public class Dog implements Pet {

    @Override
    public void feed() {
      System.out.println("Feed the Dog");
    }
    
}
