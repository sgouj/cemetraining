package com.allstate.DI;

public class Cat implements Pet {
    @Override
    public void feed() {
        System.out.println("Feed the cat");
    }
}
